from django.conf.urls import url
from django.urls import path
from .views import PacMan

app_name = "pacman"


urlpatterns = [
    path("pac_man", PacMan.as_view(), name="pacman")
]
