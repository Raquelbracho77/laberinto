from django.shortcuts import render
from django.views.generic import TemplateView
from .textos import diccionario
from pprint import pprint

# Create your views here.

class IndiceLeyendasView(TemplateView):
    template_name = "leyenda/index.dj.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({"leyendas":diccionario})
        return context


class LeyendaView(TemplateView):
    template_name = "leyenda/texto.dj.html"


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        nombre = context.get('nombre')
        leyenda = diccionario.get(nombre,{})
        if not leyenda:
            leyenda = {"texto":f"No existe leyenda {nombre}"}
        pprint(leyenda)
        context.update(leyenda)
        return context


