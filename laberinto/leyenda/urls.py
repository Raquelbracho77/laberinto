"""  """
from django.conf.urls import url
from django.urls import path
from .views import IndiceLeyendasView, LeyendaView




app_name =  "leyenda"

urlpatterns = [
    path("indice", IndiceLeyendasView.as_view(), name="indice"),
    path("nombre=<slug:nombre>/", LeyendaView.as_view(), name="nombre"),
]
