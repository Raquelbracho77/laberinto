from django.shortcuts import render
from django.views.generic import TemplateView


# Create your views here.
class LaberintoIcaro(TemplateView):
    template_name = "lab_icaro/index.dj.html"
